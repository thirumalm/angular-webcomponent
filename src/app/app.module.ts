import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './components/product-component/product.component';
import { createCustomElement } from '@angular/elements';
@NgModule({
  declarations: [
    AppComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  entryComponents: [ProductComponent]
})
export class AppModule {
  constructor(private injector:Injector){

  }
  public ngDoBootstrap(){
    console.log('ngDoBootstrap : hooks');
    const el = createCustomElement(ProductComponent, {injector: this.injector});;
    customElements.define('app-product-component',el);
  }
}
