const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/angular-webcomponent/runtime-es2015.js',
    './dist/angular-webcomponent/polyfills-es2015.js',
    './dist/angular-webcomponent/main-es2015.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/angular-webcomponent.js');
  await fs.copyFile(
    './dist/angular-webcomponent/styles.css',
    'elements/styles.css'
  );
})();